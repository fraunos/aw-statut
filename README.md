# Statut Aeroklubu Warszawskiego

To repozytorium zawiera statut w wersjach:
1. opublikowanej w .pdf
2. przetworzonej programem pdftotext w .txt
3. ręcznie sformatowanej z txt do markdown .md

Umieszczenie statutu w repozytorium programu kontroli wersji git pozwala łatwo śledzić, proponować i wprowadzać zmiany.
4 marca 2019

## STATUT AEROKLUBU WARSZAWSKIEGO

Niniejszy statut jest statutem **Aeroklubu Warszawskiego** (dalej: „Aeroklub”).

Polskie lotnictwo sportowe, skupione w Aeroklubie, jest trwałym elementem historii i kultury narodu.

Dzięki zaangażowaniu społeczności lotniczej, dla której lotnictwo stało się pasją życia i służby, działalność Aeroklubu zdobyła wysokie uznanie w kraju i na arenie międzynarodowej.

Aeroklub organizacja społeczna, spadkobierca pięknych tradycji wielu lotniczych pokoleń Polaków, pragnie wzbogacić te tradycje o nowe wartości.

### ROZDZIAŁ I
#### POSTANOWIENIA OGÓLNE.

§ 1. 

1. Aeroklub Warszawski (dalej: „Aeroklub”) jest stowarzyszeniem i posiada osobowość prawną.
2. W kontaktach międzynarodowych Aeroklub obok nazwy polskiej, może używać tłumaczenia nazwy w języku angielskim.

§ 2.

1. Terenem działania Aeroklubu jest obszar Rzeczypospolitej Polskiej, a dla właściwego realizowania swych celów Aeroklub może także prowadzić działalność poza jej granicami.
2. Siedzibą władz Aeroklubu jest miasto Warszawa.
3. Nazwa Aeroklubu może być prawnie zastrzeżona.

§ 3.

1. Aeroklub samodzielnie określa swoje cele, programy działania i struktury organizacyjne oraz uchwala akty wewnętrzne określające jego działalność.
2. Aeroklub może mieć własny emblemat, flagę, proporzec i znaki towarowe oraz używać pieczęci, ubiorów i odznak organizacyjnych.
3. Aeroklub może posiadać status organizacji pożytku publicznego.
4. Aeroklub może realizować swoje cele przy pomocy Aeroklubu Polskiego, innych aeroklubów i szkół lotniczych, stworzonych przez siebie spółek i innych podmiotów zależnych a także współdziałać z innymi organizacjami o zbieżnych do swoich celach.
5. Aeroklub opiera swoją działalność na pracy społecznej członków.
6. Do prowadzenia swoich spraw Aeroklub może zatrudniać pracowników.
7. Aeroklub może prowadzić działalność gospodarczą oraz działalność rolniczą w celu pozyskania środków na działalność statutową. Działalność regulowaną, w tym wymagającą zezwoleń lub koncesji aeroklub prowadzi zgodnie z odpowiednimi przepisami regulującymi taką działalność.

§ 4.

1. Aeroklub jest neutralny pod względem politycznym, religijnym, etnicznym i rasowym.
2. Aeroklub może być członkiem krajowych i międzynarodowych organizacji o tych samych lub podobnych założeniach programowych.
3. Aeroklub działa w oparciu o niniejszy statut oraz zgodnie z obowiązującymi przepisami prawa.
4. Aeroklub może prowadzi działalność w zakresie sportu lotniczego, jako klub sportowy w rozumieniu przepisów o sporcie.

### ROZDZIAŁ 2
#### CELE I SPOSOBY ICH REALIZACJI.

§ 5.

1. Aeroklub ma następujące cele:
   1) stwarzanie warunków do prowadzenia działalności lotniczej przez członków Aeroklubu;
   2) szkolenie lotnicze i doskonalenie umiejętności lotniczych;
   3) upowszechnianie wiedzy, umiejętności i dobrych praktyk w lotnictwie, w szczególności dotyczących bezpieczeństwa lotniczego;
   4) integracja środowiska lotniczego i sympatyków lotnictwa;
   5) promocja lotnictwa;
   6) wspieranie rozwoju i upowszechnianie kultury fizycznej w lotnictwie, w szczególności w zakresie sportu lotniczego;
   7) wspieranie i rozwój infrastruktury lotniczej;
   8) reprezentowanie interesów członków Aeroklubu wobec władz instytucji krajowych i międzynarodowych;
   9) działanie na rzecz ułatwień w lotnictwie cywilnym;
   10) pielęgnowanie narodowych tradycji lotniczych, pielęgnowanie polskości, jako czynnika rozwoju świadomości narodowej, obywatelskiej i kulturowej;
   11) podnoszenie kwalifikacji zawodowych w lotnictwie oraz aktywizacja zawodowa osób pozostających bez pracy lub zagrożonych zwolnieniem z pracy;
   12) podejmowanie oraz wspieranie inicjatyw szkolenia, edukacji, wychowania, krajoznawstwa oraz wypoczynku dzieci i młodzieży w powiązaniu z lotnictwem i modelarstwem lotniczym;
   13) wspomaganie rozwoju techniki lotniczej, wynalazczości i innowacyjności w lotnictwie oraz rozpowszechnianie i wdrażanie nowych rozwiązań technicznych w działalności lotniczej;
   14) upowszechnianie wiedzy i umiejętności na rzecz obronności państwa, działanie na rzecz ratownictwa, ochrony ludności, pomocy ofiarom katastrof i klęsk żywiołowych;
   15) szkolenie i doskonalenie umiejętności członków organizacji dla potrzeb lotnictwa wojskowego i państwowego, transportu lotniczego, ratownictwa i służb państwowych;
   16) promocja i organizowanie wolontariatu w działalności lotniczej.
2. Swoje cele Aeroklub realizuje w szczególności przez wykonywanie zadań w ramach działalności nieodpłatnej:
   1) koordynacja i wspieranie działalności statutowej poszczególnych członków Aeroklubu;
   2) współdziałanie z innymi stowarzyszeniami, związkami sportowymi, organizacjami i instytucjami w kraju i za granicą;
   3) udział w naradach, zjazdach i konferencjach związanych z lotnictwem;
   4) występowanie wobec organów władzy publicznej oraz innych instytucji publicznych w sprawach będących przedmiotem działalności statutowej Aeroklubu;
   5) występowanie do władz i instytucji z poparciem inicjatyw i wniosków Aeroklubu Polskiego, innych aeroklubów oraz szkół lotniczych, związanych z działalnością statutową Aeroklubu;
   6) inicjowanie oraz opiniowanie aktów prawnych mających wpływ na działalność statutową Aeroklubu, w szczególności w zakresie sportu lotniczego i lotnictwa ogólnego oraz amatorskiego;
   7) współpracę z właściwymi władzami odpowiedzialnymi za obronność kraju;
   8) podejmowanie działań na rzecz rozwoju i utrzymania infrastruktury lotniczej;
   9) inspirowanie i popieranie postępu i twórczości technicznej, artystycznej, naukowej w dziedzinie lotnictwa;
   10) nadawanie odznak honorowych, dyplomów i innych wyróżnień;
   11) wymianę wiedzy o lotnictwie oraz dobrych praktykach lotniczych;
   12) kultywowania tradycji lotniczych, kształtowania zasad etycznych wśród członków Aeroklubu;
   13) pielęgnowania pamięci o zmarłych ludziach lotnictwa.
3. Swoje cele Aeroklub realizuje w szczególności przez wykonywanie zadań w ramach działalności odpłatnej:
   1)  udostępniania posiadanego sprzętu lotniczego oraz posiadanej infrastruktury do wykonywania czynności lotniczych przez członków Aeroklubu;
   2)  organizowania kursów teoretycznych i praktycznych w zakresie wszystkich specjalności lotniczych, pilotażowych i technicznych uprawianych w ramach struktur Aeroklubu a także wykonywanie usług lotniczych;
   3)  wydawanie i standaryzację dokumentów niezbędnych w działalności lotniczej, w szczególności instrukcji operacyjnych dla certyfikowanej działalności lotniczej;
   4)  nadzoru nad działalnością lotniczą w zakresie lotnictwa ogólnego na zasadach przewidzianych w odrębnych przepisach;
   5)  organizowanie zjazdów i konferencji związanych z lotnictwem;
   6)  organizowanie turystyki i rekreacji lotniczej;
   7)  prowadzenie działalności wydawniczej i popularyzatorskiej.

### ROZDZIAŁ 3
#### CZŁONKOWIE, ICH PRAWA I OBOWIĄZKI.

§ 6.

1. Członkiem Aeroklubu może być obywatel Rzeczypospolitej Polskiej oraz cudzoziemiec, posiadający pełną zdolność do czynności prawnych i nie pozbawiony praw publicznych, a także, jako członek wspierający, osoba prawna.
2. Małoletni w wieku 16 – 18 lat mogą należeć do stowarzyszenia i korzystać z czynnego i biernego prawa wyborczego z tym, że w składzie Zarządu Aeroklubu większość muszą stanowić osoby o pełnej zdolności do czynności prawnych.
3. Małoletni poniżej 16 lat mogą za zgodą ustawowych przedstawicieli należeć do stowarzyszenia bez prawa głosowania na walnych zebraniach członków oraz bez korzystania z czynnego i biernego prawa wyborczego do władz stowarzyszenia.
4. Członkowie Aeroklubu dzielą się na:
   1) zwyczajnych;
   2) honorowych;
   3) wspierających;
   4) stowarzyszonych.
5. Członkami zwyczajnymi mogą być osoby fizyczne pragnące czynnie realizować cele statutowe Aeroklubu.
6. Członkami honorowymi mogą zostać osoby fizyczne szczególnie zasłużone dla rozwoju lotnictwa i sportu lotniczego.
7. Członkami wspierającymi mogą zostać osoby fizyczne lub osoby prawne, które popierają działalność Aeroklubu i zadeklarują świadczenia na rzecz Aeroklubu
8. Członkami stowarzyszonymi mogą zostać osoby fizyczne, które zamierzają realizować cele Aeroklubu bez obowiązku uiszczania składek członkowskich; członkowie stowarzyszeni nie posiadają czynnego i biernego prawa wyborczego do władz oraz prawa głosowania na walnych zgromadzeniach członków.
9. Osoby nie będące obywatelami Rzeczypospolitej Polskiej i nie mające stałego miejsca zamieszkania na terytorium RP mogą być tylko członkami honorowymi lub wspierającymi Aeroklubu.

§ 7.

1. Członkowie zwyczajni, wspierający i stowarzyszeni są przyjmowani decyzją Zarządu Aeroklubu na podstawie wypełnionej deklaracji członkowskiej. Zarząd Aeroklubu może upoważnić Prezydium Zarządu do podejmowania decyzji w sprawie przyjmowania członków.
2. Godność członka honorowego Aeroklubu nadaje osobom fizycznym, za wybitną działalność w lotnictwie lub na rzecz Aeroklubu, Walne Zgromadzenie na wniosek Zarządu Aeroklubu.

§ 8.

1. Członkowie zwyczajni są obowiązani:
   1) realizować cele Aeroklubu;
   2) brać czynny udział w działalności Aeroklubu;
   3) przestrzegać postanowień statutu, uchwał, zarządzeń władz Aeroklubu oraz godnie reprezentować polskie lotnictwo sportowe;
   4) opłacać regularnie składki członkowskie.
2. Członkowie zwyczajni Aeroklubu mają prawo:
   1) wybierać i być wybieranymi do władz: Aeroklubu;
   2) korzystać z urządzeń i pomocy Aeroklubu oraz uczestniczyć w krajowych i międzynarodowych imprezach lotniczych, zgodnie z regulaminem zasad uczestnictwa;
   3) nosić ubiór i odznaki organizacyjne;
   4) korzystać ze sprzętu i infrastruktury Aeroklubu, na zasadach obowiązujących Aeroklubie.
3. Członkowie honorowi mają wszystkie prawa członków zwyczajnych oraz są zwolnieni od opłacania składek członkowskich.
4. Członkowie wspierający mają obowiązek:
   1) propagować cele statutowe Aeroklubu;
   2) regularnie opłacać składki członkowskie oraz wywiązywać się z zadeklarowanych świadczeń.
5. Członkowie wspierający mają prawa członków zwyczajnych, z wyjątkiem biernego i czynnego prawa wyborczego.
6. Zarząd Aeroklubu ma prawo ustalać dodatkowe prawa i związane z nimi obowiązki, dotyczące członków Aeroklubu.
7. Członkowie Aeroklubu za aktywny udział w realizacji celów Aeroklubu mogą być wyróżniani dyplomami uznania i odznakami honorowymi. Zasady wyróżniania oraz wzory odznak i wyróżnień ustala Zarząd Aeroklubu.
8. Członkowie Aeroklubu, którzy działają na szkodę stowarzyszenia bądź naruszają postanowienia statutu lub innych wewnętrznych przepisów Aeroklubu, podlegają karom organizacyjnym:
   1) upomnienia;
   2) nagany;
   3)  zawieszenia w prawach członka na okres do dwóch lat;
   4)  wykluczenia z Aeroklubu.
9. O nałożeniu kar organizacyjnych określonych w ust.8. pkt.1 – 3. decydują sądy koleżeńskie z inicjatywy własnej lub na wniosek Zarządu Aeroklubu. O nałożeniu kary określonej w punkcie 4. decydują sądy koleżeńskie na wniosek Zarządu Aeroklubu. Od decyzji sądu koleżeńskiego ukaranemu członkowi, jak również Zarządowi Aeroklubu, przysługuje prawo odwołania do Walnego Zgromadzenia Aeroklubu w terminie 30 dni od dnia otrzymania decyzji. Odwołanie kieruje się na ręce Sądu Koleżeńskiego, który obowiązany jest przekazać je do Zarządu zaś Zarząd obowiązany jest przedstawić odwołanie do rozpoznania na najbliższym Walnym Zgromadzeniu.
10. Członkowie Aeroklubu, naruszający przepisy i regulaminy sportowe podlegają karom sportowym ustalonym na zasadach wynikających z przepisów o sporcie.

§ 9.

Przynależność do Aeroklubu ustaje w razie: 
   1) śmierci członka;
   2) dobrowolnego wystąpienia, o czym członek ma obowiązek zawiadomić pisemnie Zarząd Aeroklubu z trzydziestodniowym wyprzedzeniem;
   3) skreślenia z listy członków przez Zarząd z powodu nie usprawiedliwionego zalegania z opłatą składki członkowskiej ponad 6 miesięcy;
   4) skreślenia z listy członków wspierających przez Zarząd Aeroklubu w razie nie wywiązywania się z zadeklarowanych świadczeń;
   5) skreślenie z listy członków stowarzyszonych przez Zarząd Aeroklubu w razie nie realizowania celów Aeroklubu;
   6) uprawomocnienia decyzji o wykluczeniu z Aeroklubu;
   7) utraty praw publicznych.

### ROZDZIAŁ 4
#### ZASADY DZIAŁANIA AEROKLUBU

§ 10.

1. Wybory władz Aeroklubu odbywają się w głosowaniu tajnym.
2. Kadencja władz Aeroklubu trwa cztery lata.
3. Członkowie wybierani do władz Aeroklubu mogą być jednocześnie członkami tylko jednego organu tj. Zarządu, Komisji Rewizyjnej lub Sądu Koleżeńskiego. Członek Zarządu Aeroklubu nie może być osobą, która była skazana prawomocnym wyrokiem za umyślne przestępstwo lub umyślne przestępstwo skarbowe ścigane z oskarżenia publicznego.
4. Członkami Komisji Rewizyjnej Aeroklubu mogą być tylko osoby, nie będące pracownikami Aeroklubu.
5. Uchwały władz Aeroklubu podejmowane są bezwzględną większością głosów w obecności, co najmniej ½ ogólnej liczby osób uprawnionych do głosowania.
6. Uchwały w sprawie zmian statutu wymagają większości 2/3 głosów w obecności co najmniej 1/4 ogólnej liczby osób uprawnionych do głosowania.
7. Uchwały w sprawie wniosku o rozwiązanie Aeroklubu wymagają większości ¾ głosów w obecności co najmniej 2/3 ogólnej liczby osób uprawnionych do głosowania.
8. Uchwały podejmuje się w głosowaniu jawnym. Na żądanie większości uprawnionych do głosowania, podjęcie uchwały może odbywać się w głosowaniu tajnym.

### ROZDZIAŁ 5
#### WŁADZE AEROKLUBU.

§ 11.

Władzami Aeroklubu są:
   1) Walne Zgromadzenie (zgromadzenie delegatów);
   2) Zarząd;
   3) Komisja Rewizyjna;
   4) Sąd Koleżeński.

##### A. WALNE ZGROMADZENIE AEROKLUBU.

§ 12.

1. Walne Zgromadzenie jest najwyższą władzą Aeroklubu.
2. Walne Zgromadzenie może być zwyczajne lub nadzwyczajne.
3. Walne Zgromadzenie sprawozdawcze jest zwoływane nie rzadziej niż raz na rok, a zwyczajne sprawozdawczo-wyborcze raz na cztery lata.
4. Nadzwyczajne Walne Zgromadzenie jest zwoływane na wniosek:
   1) Zarządu Aeroklubu;
   2) Komisji rewizyjnej Aeroklubu;
   3) co najmniej 1/3 ogólnej liczby członków zwyczajnych, zgłoszony na piśmie do zarządu Aeroklubu.
5. Nadzwyczajne Walne Zgromadzenie powinno się odbyć najpóźniej w terminie 30 dni od dnia złożenia wniosku.
6. Walne Zgromadzenie zwołuje Zarząd, który pisemnie lub drogą elektroniczną zawiadamia o tym członków zwyczajnych, honorowych i wspierających, będących osobami fizycznymi podając termin, miejsce i porządek obrad co najmniej na 14 dni przed zgromadzeniem.
7. W przypadku gdy liczba członków Aeroklubu przekracza 150 osób, zamiast Walnego Zgromadzenia mogą odbywać się zgromadzenia delegatów. Delegaci są wówczas wybierani w liczbie proporcjonalnej do liczby członków zwyczajnych i honorowych Aeroklubu w stosunku 1 delegat na 5 członków. Kadencja delegatów ustaje z chwilą zakończenia obrad Walnego Zgromadzenia, na które zostali wybrani.
8. W Walnym Zgromadzeniu udział biorą, z głosem decydującym, członkowie zwyczajni i honorowi, którzy ukończyli 16 lat, a także z głosem doradczym osoby zaproszone. W Zgromadzeniu Delegatów udział biorą, z głosem decydującym, delegaci członków, a z głosem doradczym – członkowie Zarządu, Komisji Rewizyjnej i Sądu Koleżeńskiego, jeżeli nie są delegatami oraz osoby zaproszone.
9. Walne Zgromadzenie jest prawomocne w pierwszym terminie w obecności co najmniej ½ ogólnej liczby członków (delegatów), a w drugim terminie – bez względu na liczbę obecnych członków (delegatów).

§ 13.

Do Walnego Zgromadzenia (Zgromadzenia Delegatów) należy w szczególności:
   1) ustalanie programów działalności Aeroklubu;
   2) rozpatrywanie i zatwierdzanie sprawozdań zarządu aeroklubu oraz sprawozdań i wniosków komisji rewizyjnej i sądu koleżeńskiego;
   3) wybór członków zarządu, komisji rewizyjnej i sądu koleżeńskiego;
   4) udzielenie absolutorium ustępującemu zarządowi;
   5) rozpatrywanie innych spraw przekazanych walnemu zgromadzeniu przez zarząd, komisję rewizyjną lub sąd koleżeński oraz wniosków i odwołań zgłoszonych przez członków Aeroklubu co najmniej 14 dni przed terminem walnego zgromadzenia;
   6) podjęcie uchwały o rozwiązaniu Aeroklubu;
   7) udzielenie zgody na dysponowanie nieruchomościami przekraczające zakres zwykłego zarządu
   8) uchwalanie zmian statutu Aeroklubu.

§ 14.

Nadzwyczajne Walne Zgromadzenie (Zgromadzenie Delegatów) rozpatruje sprawy, które stanowiły cel jego zwołania oraz podejmuje w tym zakresie uchwały.

##### B. ZARZĄD AEROKLUBU.

§ 15.

1. W okresie pomiędzy walnymi zgromadzeniami najwyższą władzą Aeroklubu jest Zarząd, który odpowiada za swoją działalność przed Walnym Zgromadzeniem (zgromadzeniem delegatów).
2. Do Zarządu Aeroklubu należy w szczególności:
   1) realizowanie uchwał Walnego Zgromadzenia;
   2) opracowanie programów działalności Aeroklubu oraz przyjmowanie sprawozdań z ich realizacji;
   3) zatwierdzanie preliminarza budżetowego i bilansu;
   4) podejmowanie uchwał w sprawach majątkowych oraz w zakresie działalności gospodarczej;
   5) powoływanie i rozwiązywanie sekcji specjalistycznych i innych ogniw, uchwalanie ich regulaminów oraz nadzorowanie i koordynowanie ich działalności;
   6) zwoływanie walnych zgromadzeń;
   7) powoływanie i odwoływanie dyrektora Aeroklubu oraz ustalanie jego zakresu obowiązków i wynagrodzenia;
   8) ustalanie wysokości i zasad płacenie składki członkowskiej;
   9) określanie liczebności, struktury i regulaminu organizacyjnego Aeroklubu;
   10) powoływanie i odwoływanie społecznego przewodniczącego zespołu bezpieczeństwa lotniczego,

§ 16.

1. W skład Zarządu Aeroklubu wchodzą osoby wybrane przez Walne Zgromadzenie(zgromadzenie delegatów) w liczbie ustalonej przez Walne Zgromadzenie. Przewodniczący sekcji specjalnościowych, o ile nie są z wyboru członkami Zarządu Aeroklubu, uczestniczą w posiedzeniach Zarządu z głosem doradczym.
2. Zarząd może dokooptować do swojego składu nowych członków na wakujące miejsca w liczbie nie większej niż 40% liczby wybranych członków. W przypadku, gdy w trakcie kadencji ilość członków Zarządu spadnie poniżej 60% wybranych, Zarząd obowiązany jest zwołać Walne Zgromadzenie celem dokonania wyborów uzupełniających. Kadencja tak wybranych członków trwa do końca kadencji pierwotnie wybranego Zarządu.
3. Posiedzenia Zarządu odbywają się w miarę potrzeb, nie rzadziej jednak niż raz na trzy miesiące.
4. Dyrektor Aeroklubu, jeżeli nie jest z wyboru członkiem Zarządu Aeroklubu uczestniczy w posiedzeniach Zarządu i Prezydium Zarządu z głosem doradczym.

§ 17.

1. Zarząd Aeroklubu wybiera spośród swoich członków Prezydium w liczbie przez siebie ustalonej, w tym: Prezesa, Wiceprezesa, Sekretarza i Skarbnika.
2. Prezydium Zarządu działa w imieniu Zarządu w okresach między posiedzeniami Zarządu.
3. Posiedzenia Prezydium zwołuje Prezes lub z jego upoważnienia Wiceprezes w miarę potrzeby.
4. Zadania Prezydium oraz zakres jego uprawnień ustala Zarząd Aeroklubu.

§ 18.

1. Dyrektor Aeroklubu kieruje bieżącą pracą Aeroklubu i jest przełożonym wszystkich osób zatrudnionych w Aeroklubie w celu realizacji zadań Aeroklubu.
2. Dyrektor Aeroklubu w zakresie realizacji zadań statutowych i uchwał Zarządu Aeroklubu odpowiada przed Zarządem Aeroklubu.

##### C. KOMISJA REWIZYJNA AEROKLUBU

§ 19.

1. Komisja Rewizyjna kontroluje całokształt działalności statutowej Aeroklubu i składa sprawozdania ze swojej działalności przed Walnym Zgromadzeniem (zgromadzeniem delegatów).
2. Komisja Rewizyjna Aeroklubu składa się z minimum 3 członków – w liczbie ustalonej przez Walne Zgromadzenie. Komisja Rewizyjna wybiera ze swego grona Przewodniczącego, zastępcę Przewodniczącego i Sekretarza. Komisja Rewizyjna może dokooptować do swojego składu nowych członków na wakujące miejsca w liczbie nie większej niż 1/3 wybranej liczby członków.
3. Posiedzenia Komisji Rewizyjnej odbywają się w miarę potrzeby, nie rzadziej jednak niż raz na sześć miesięcy.
4. Do zadań Komisji Rewizyjnej należy:
   1) przeprowadzanie przynajmniej raz do roku kontroli całokształtu działalności statutowej Aeroklubu;
   2) przedstawianie Zarządowi Aeroklubu wniosków wynikających z ustaleń kontroli i żądanie usunięcia stwierdzonych uchybień;
   3) występowanie z wnioskiem o udzielenie absolutorium ustępującemu Zarządowi Aeroklubu.
5. Delegowany członek Komisji Rewizyjnej bierze udział w posiedzeniach Zarządu i Prezydium Zarządu.

##### D. SĄD KOLEŻEŃSKI AEROKLUBU.

§ 20.

1. Sąd Koleżeński składa się z minimum 3 członków – w liczbie ustalonej przez Walne Zgromadzenie (zgromadzenie delegatów).
2. Sąd Koleżeński, wybiera spośród swego grona Przewodniczącego, Wiceprzewodniczącego i Sekretarza. Sąd Koleżeński może dokooptować do swojego składu nowych członków na wakujące miejsca w liczbie nie większej niż 1/3 wybranej liczby członków.
3. Sąd Koleżeński rozpoznaje w co najmniej trzyosobowych zespołach orzekających sprawy dotyczące postępowania członków naruszających statut Aeroklubu oraz działających na szkodę stowarzyszenia. Od postanowień Sądu Koleżeńskiego przysługuje odwołanie do Walnego Zgromadzenia Aeroklubu.
5. Posiedzenia Sądu Koleżeńskiego odbywają się w miarę potrzeby.
6. Sąd Koleżeński składa sprawozdania ze swojej działalności Walnemu Zgromadzeniu (zgromadzeniu delegatów).

##### E. OGNIWA ORGANIZACYJNE AEROKLUBU.

§ 21.

1. Podstawowymi ogniwami organizacyjnymi Aeroklubu są sekcje specjalistyczne oraz afiliowane Kluby Seniorów Lotnictwa.
2. Do powołania sekcji jest potrzebnych co najmniej 5 członków.

§ 22.

1. Sekcje specjalistyczne są tworzone w Aeroklubie stosownie do uprawianych dyscyplin sportu lotniczego lub według zainteresowań członków Aeroklubu.
2. Sekcje specjalistyczne działają zgodnie z regulaminem ustalonym przez Zarząd Aeroklubu.
3. Sekcje specjalistyczne są ogniwem Aeroklubu posiadającym uprawnienie do wyboru delegatów na Walne Zgromadzenie Delegatów Aeroklubu. Kandydaci na delegatów nie muszą być członkami sekcji specjalistycznych dokonujących wyborów.

§ 23.

1. Zarząd Aeroklubu może tworzyć i rozwiązywać inne ogniwa organizacyjne jak kluby i koła specjalistyczne w środowiskach pracy, nauki, w miejscach zamieszkania, szkołach oraz w ramach innych stowarzyszeń i organizacji, przestrzegając przepisów prawa i statutów oraz uwzględniając zainteresowania chętnych.
2. Powoływane ogniwa organizacyjne działają zgodnie z regulaminami ustalonymi przez Zarząd Aeroklubu.

### ROZDZIAŁ 6
#### PRAWA MAJĄTKOWE I NIEMAJĄTKOWE. DZIAŁALNOŚĆ GOSPODARCZA.

§ 24.

1. Aeroklub prowadzi działalność gospodarczą zgodnie z Prawem o stowarzyszeniach i innymi obowiązującymi w tym zakresie przepisami w szczególności prowadzi działalność gospodarczą służącą pozyskiwaniu środków na realizacje celów statutowych i w rozmiarach służących realizacji celów statutowych.
2. Aeroklub może prowadzić działalność gospodarczą obejmującą w szczególności:
   1) pielęgnowanie, zraszanie, opryskiwanie upraw, włączając usługi agrolotnicze,
   2) loty czarterowe,
   3) loty widokowe i wycieczkowe,
   4) wynajem środków lotniczego transportu pasażerskiego z załogą,
   5) działalność lotnictwa ogólnego, taką jak: przewozy pasażerów organizowane przez
   6) aerokluby w celach szkoleniowych lub dla przyjemności w ramach organizacji szkolenia,
   7) transport lotniczy towarów, nieobjęty rozkładem lotów,
   8) wynajem środków transportu lotniczego z załogą, w celu przewozu towarów,
   9) działalność usługową wspomagającą lotniczy transport pasażerów, zwierząt i towarów,
   10) działalność terminali, takich jak: porty, dworce lotnicze itp.,
   11) obsługa naziemna statków powietrznych itp.,
   12) fotografię lotniczą,
   13) wynajem i dzierżawę środków transportu lotniczego bez załogi m.in.: samolotów,
   14) balonów,
   15) inne usługi lotnicze,
   16) obrót paliwami,
   17) prowadzenie działalności rolniczej.
1. Cały dochód z działalności gospodarczej przeznaczany jest na działalność statutową.

### ROZDZIAŁ 7
#### KLUB SENIORÓW LOTNICTWA.

§ 25.

1. Przy Aeroklubie może działać Klub Seniorów Lotnictwa.
2. Klub Seniorów Lotnictwa jest autonomiczną organizacją posiadającymi swoje władze orazstrukturę organizacyjną. Wobec władz Aeroklubu Klub Seniorów Lotnictwa reprezentowany jest przez swoją władzę wykonawczą. Afiliacja regionalnego Klubu Seniorów Lotnictwa do aeroklubu następuje na mocy uchwały zarządu tego Aeroklubu, na wniosek Klubu Seniorów Lotnictwa.
3. Klub Seniorów Lotnictwa zrzesza długoletnich członków Aeroklubu, pracowników i działaczy lotnictwa oraz inne osoby zasłużone dla lotnictwa, spełniające regulaminowe kryteria senioratu. Członkowie Klub Seniorów Lotnictwa afiliowanego do Aeroklubu są członkami stowarzyszonymi. Mogą być też członkami zwyczajnymi i honorowymi Aeroklubu, ze wszystkimi związanymi prawami i obowiązkami.
4. Klub Seniorów Lotnictwa realizuje zadania stowarzyszenia, a w szczególności:
   1) współdziała z Aeroklubem w pracy wychowawczej i szkoleniowej;
   2) propaguje idee lotnictwa i tradycje lotnicze;
   3) gromadzi i opracowuje materiały historyczne z dziedziny lotnictwa;
   4) pielęgnuje pamięć o zmarłych ludziach lotnictwa.
5. Zasady współdziałania Klubu Seniorów Lotnictwa z Aeroklubem określa regulamin zatwierdzony przez Zarząd Aeroklubu.

### ROZDZIAŁ 8
#### MAJĄTEK AEROKLUBU. ZASADY REPREZENTACJI.

§ 26.

1. Dysponowanie majątkiem Aeroklubu, w tym zbywanie i obciążanie, leży w wyłącznej gestii Zarządu Aeroklubu, przy czym dysponowanie nieruchomościami Aeroklubu przekraczające zakres zwykłego zarządu, wymaga podjęcia decyzji w formie uprzedniej uchwały Walnego Zgromadzenia Aeroklubu.
2. Majątek Aeroklubu stanowią nieruchomości, ruchomości, fundusze oraz prawa majątkowe.
3. Na fundusze Aeroklubu składają się w szczególności:
   1) dochody z organizowanych imprez, szkolenia lotniczego i wydawnictw;
   1) darowizny, zapisy i spadki;
   2) dochody osiągane z działalności gospodarczej;
   3) dotacje;
   4) dochody z ofiarności społecznej;
   5) dochody z majątku własnego;
   6) składki członkowskie.
4. Do ważności umów, zobowiązań, pełnomocnictw i innych oświadczeń woli wymagane są podpisy Prezesa lub Wiceprezesa oraz Dyrektora Aeroklubu lub dwóch członków Zarządu, w tym Prezesa lub Wiceprezesa Zarządu.
5. Dokumenty i pisma związane z bieżącą działalnością Aeroklubu podpisuje Dyrektor Aeroklubu oraz Prezes lub Wiceprezes.
6. W przypadku uzyskania statusu organizacji pożytku publicznego zabrania się:
   1) udzielania pożyczek lub zabezpieczania zobowiązań majątkiem Aeroklubu w stosunku do ich członków, członków władz lub pracowników oraz ich osób bliskich w rozumieniu ustawy z 24 kwietnia 2003r. o działalności pożytku publicznego i o wolontariacie;
   2) przekazywania majątku Aeroklubu na rzecz ich członków, członków władz lub pracowników oraz ich osób bliskich, na zasadach innych niż w stosunku do osób trzecich, w szczególności, jeżeli przekazanie to następuje bezpłatnie lub na preferencyjnych warunkach;
   3) wykorzystywania majątku na rzecz członków, członków władz lub pracowników oraz ich osób bliskich na zasadach innych niż w stosunku do osób trzecich, chyba, że to wykorzystanie bezpośrednio wynika ze statutowego celu Aeroklubu;
   4)  zakupu na szczególnych zasadach towarów lub usług od podmiotów, w których uczestniczą członkowie stowarzyszenia, członkowie władz Aeroklubu, szkół lotniczych, pracownicy oraz ich osoby bliskie.

§ 27.

W przypadku upadłości lub likwidacji rozwiązania Aeroklubu jego majątek, w zakresie nie rozdysponowanym na podstawie obowiązujących przepisów prawa, zostaje przeznaczony na cele określone w uchwale Walnego Zgromadzenia o likwidacji Aeroklubu.

§ 28.

Jeżeli Walne Zgromadzenie nie przyjmie tekstu jednolitego statutu po zmianach, taki tekst jednolity Statutu przeznaczony do złożenia w sądzie rejestrowym sporządza Zarząd Aeroklubu przyjmując go w formie uchwały podjętej nie później niż w terminie 30 dni od dnia chwalenia każdej zmiany Statutu.
